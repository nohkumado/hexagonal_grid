import 'package:hexagonal_grid/hexmap.dart';
import '../example/example.dart';

void main() {
  final hexmap = Hexmap.hexagon(radius: 10, maxheight: 5);
  //hexmap.map.remove('0,0'); // Example: Remove a hex from the map
  print(hexmap);
  Example example = Example();
  example.createOriginHexWithLayout();
  example.createNeighbors();
}
