# hexagonal grid
  fork of [Jonathan Beckman's](mailto:jonathantbeckman@gmail.com)[gitlab](https://gitlab.com/nohkumado/hexagonal_grid)
  
A hexagonal grid library for Dart  that uses axial coordinates and contains algorithms to build hex
layouts. Inspired and implemented from https://www.redblobgames.com/grids/hexagons/implementation.html
This library is intended as a base for game mechanics, as described in the redblobgames papers.

Some breaking changes were made since version 1.0.5, with consent of the original author i will
continue with a parallel dev-tree


addition of Hexmap, there is a factory constructor for the map, for a regular
hexagon map:

```
Hexmap hexagonal = Hexmap.hexagon(radius :3);
--------------- HexMap of 6x6 ----------------
...............(0,-3)  (1,-3)  (2,-3)  (3,-3)  
..........(-1,-2)  (0,-2)  (1,-2)  (2,-2)  (3,-2)  
.....(-2,-1)  (-1,-1)  (0,-1)  (1,-1)  (2,-1)  (3,-1)  
(-3,0)  (-2,0)  (-1,0)  (0,0)  (1,0)  (2,0)  (3,0)  
.....(-3,1)  (-2,1)  (-1,1)  (0,1)  (1,1)  (2,1)  
..........(-3,2)  (-2,2)  (-1,2)  (0,2)  (1,2)  
...............(-3,3)  (-2,3)  (-1,3)  (0,3)  
```
for a  "squashed" map, with restrained height:

```
Hexmap    hexagonal = Hexmap.hexagon(radius :3, maxheight: 2);
second test --------------- HexMap of 6x4 ----------------
..........(-1,-2)  (0,-2)  (1,-2)  (2,-2)  (3,-2)  
.....(-2,-1)  (-1,-1)  (0,-1)  (1,-1)  (2,-1)  (3,-1)  
(-3,0)  (-2,0)  (-1,0)  (0,0)  (1,0)  (2,0)  (3,0)  
.....(-3,1)  (-2,1)  (-1,1)  (0,1)  (1,1)  (2,1)  
..........(-3,2)  (-2,2)  (-1,2)  (0,2)  (1,2)  
```

Added terrain info to the map, driven by perlin noise

Other wise look into the unit tests on how to use this!
or drop me a mail, i am using this to try to implement a
xconq standard game sort of... so i some body wants the flutter stuff,
drop me a mail, ATM i don't have the drive to create another separate
package only for the hexmap stuff....



# To Publish to Pub
* Follow https://www.dartlang.org/tools/pub/publishing

--- or ---

* Make updates to code
* Update `pubspec.yaml` to the desired version number
* Update `CHANGELOG.md` with the version number and changes
* `pub publish --dry-run`
* `pub publish`
