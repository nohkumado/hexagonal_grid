## [1.0.6] - 01/11/2024
- rewrote a lot of the stuff i didn't understand, added factory methods to create a hex mesh, rewrote the storage as it somehow didn't work for me, added terrain hexes, added tests for i hope everything

## [1.0.5] - 03/01/2019

* Bug fix for toSpiralHexLayout with a null or empty list

## [1.0.4] - 01/16/2019

* General clean up and make package available for Dart projects, not just Flutter

## [1.0.0] - 01/16/2019

* Packaged this hex grid code that I have been using in my own projects so other projects and
packages can use it. Contains implementations for Hex via axial coordinates as well as layouts and
an algorithm to create a spiral out layout given a list.
