import 'hex.dart';

class OffsetCoordinate {
  int col;
  int row;

  OffsetCoordinate(this.col, this.row);
  factory OffsetCoordinate.toOffsetOddR(Hex hex) {
    int col = hex.q + (hex.r - (hex.r % 2)) ~/ 2;
    int row = hex.r;
    return new OffsetCoordinate(col.toInt(), row);
  }
  factory OffsetCoordinate.toOffsetEvenR(Hex hex) {
    int col = hex.q + (hex.r + (hex.r % 2)) ~/ 2;
    int row = hex.r;
    return new OffsetCoordinate(col, row);
  }
  factory OffsetCoordinate.toOffsetOddQ(Hex hex) {
    int row = hex.r + (hex.q - (hex.q % 2)) ~/ 2;
    int col = hex.q;
    return new OffsetCoordinate(col, row);
  }
  factory OffsetCoordinate.toOffsetEvenQ(Hex hex) {
    int row = hex.r + (hex.q + (hex.q % 2)) ~/ 2;
    int col = hex.q;
    return new OffsetCoordinate(col, row);
  }
}
