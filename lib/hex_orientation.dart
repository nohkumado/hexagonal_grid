import 'dart:math';

class HexOrientation {
  final double f0, f1, f2, f3;
  final double b0, b1, b2, b3;
  final double startAngle;

  const HexOrientation._({
    required this.f0,
    required this.f1,
    required this.f2,
    required this.f3,
    required this.b0,
    required this.b1,
    required this.b2,
    required this.b3,
    required this.startAngle,
  });

  static const double sqrt3 =
      1.7320508075688772; // Manually calculated value for sqrt(3.0)

  factory HexOrientation.pointy() {
    return const HexOrientation._(
      f0: sqrt3,
      f1: sqrt3 / 2.0,
      f2: 0.0,
      f3: 3.0 / 2.0,
      b0: sqrt3 / 3.0,
      b1: -1.0 / 3.0,
      b2: 0.0,
      b3: 2.0 / 3.0,
      startAngle: -pi/6,
    );
  }

  factory HexOrientation.flat() {
    return const HexOrientation._(
      f0: 3.0 / 2.0,
      f1: 0.0,
      f2: sqrt3 / 2.0,
      f3: sqrt3,
      b0: 2.0 / 3.0,
      b1: 0.0,
      b2: -1.0 / 3.0,
      b3: sqrt3 / 3.0,
      startAngle: 0.0,
    );
  }

  @override
  String toString() {
    return '${startAngle == 0.0 ? 'flat' : 'pointy'}';
  }
}
