import 'hex.dart';

enum Terrains {
  OCEAN,
  WATER,
  SHALLOWS,
  BEACH,
  PLAIN,
  HILLS,
  MOUNTAIN,
  SNOW,
  LIMIT,
}

enum Biomes implements Comparable<Biomes> {
  WATER(water: 50, wood: 0, fuel: 5),
  DESERT(water: 1, wood: 0, fuel: 10),
  SCORCHED(water: 5, wood: 0, fuel: 5),
  BARE(water: 6, wood: 0, fuel: 1),
  SAVANNAH(water: 7, wood: 1, fuel: 1),
  GRASSLAND(water: 20, wood: 1, fuel: 1),
  TUNDRA(water: 21, wood: 5, fuel: 1),
  FOREST(water: 25, wood: 20, fuel: 1),
  RAIN_FOREST(water: 30, wood: 20, fuel: 1),
  TAIGA(water: 21, wood: 20, fuel: 1),
  JUNGLE(water: 50, wood: 20, fuel: 1),
  MOUNTAIN(water: 20, wood: 5, fuel: 0);

  const Biomes({required this.water, required this.wood, required this.fuel});
  final water;
  final wood;
  final fuel;

  double get food => (water < 30) ? water - .25 * wood : water + .25 * wood;
  @override
  int compareTo(Biomes other) => water - other.water;
}

class TerrainHex extends Hex {
  double elevation = -1.0;
  double bionoise = -10.0;
  double waterlevel = 0.0;
  Terrains terrain = Terrains.PLAIN;
  Biomes vegetation = Biomes.GRASSLAND;
  bool valid = true;

  TerrainHex({required super.q, required super.r});

  void addNoise(
      {required double noise,
      int fudge = 1,
      double waterlevel = 0.11,
      required double bionoise}) {
    this.waterlevel = waterlevel;
    //print("adding noise $noise to terrain");
    //simple noise map
    elevation = addElevation(seed: noise);
    biome(noise: bionoise, waterlvl: waterlevel);
  }

  @override
  clone({required int q, required int r}) {
    return TerrainHex(q: q, r: r);
  }

  double addElevation({double? seed, double? inc}) {
    if (seed != null) elevation = seed;
    if (inc != null) elevation += inc;
    return elevation;
    //supposedly better terrain map map but gives 0....
    //print(" test of pow ${pow(2,1)} and pow ${pow(2,2)}");
    //elevation = pow(elevation,fudge).toDouble();
    //print("adding elevation $elevation");
  }

  void biome({double? waterlvl, double? noise}) {
    //print("computing biomes for $elevation water: $waterlvl");
    if (noise != null) bionoise = noise;
    if (waterlvl != null) this.waterlevel = waterlvl;
    if (bionoise < 0) {
      if (elevation < 0.1) {
        terrain = Terrains.WATER;
        vegetation = Biomes.WATER;
      } else if (elevation < 0.2)
        terrain = Terrains.BEACH;
      else if (elevation < 0.3)
        terrain = Terrains.PLAIN;
      else if (elevation < 0.5)
        terrain = Terrains.HILLS;
      else if (elevation < 0.7) {
        terrain = Terrains.MOUNTAIN;
        vegetation = Biomes.BARE;
      } else if (elevation < 0.9)
        terrain = Terrains.SNOW;
      else
        terrain = Terrains.LIMIT;
      return;
    }

    if (elevation < waterlevel + .015) {
      vegetation = Biomes.WATER;
      if (elevation < waterlevel - .01)
        terrain = Terrains.OCEAN;
      else if (elevation < waterlevel + .01)
        terrain = Terrains.SHALLOWS;
      else
        terrain = Terrains.BEACH;
      return;
    }

    if (elevation > waterlevel + .69) {
      terrain = Terrains.MOUNTAIN;
      vegetation = Biomes.MOUNTAIN;
      if (bionoise < 0.1)
        vegetation = Biomes.SCORCHED;
      else if (bionoise < 0.2)
        vegetation = Biomes.BARE;
      else if (bionoise < 0.5)
        vegetation = Biomes.TUNDRA;
      else
        terrain = Terrains.SNOW;
      return;
    }

    if (elevation > waterlevel + 0.49) {
      terrain = Terrains.HILLS;
      if (bionoise < 0.33)
        vegetation = Biomes.DESERT;
      else if (bionoise < 0.66)
        vegetation = Biomes.TUNDRA;
      else
        vegetation = Biomes.TAIGA;
      return;
    }

    if (elevation > waterlevel + 0.19) {
      terrain = Terrains.PLAIN;
      if (bionoise < 0.16)
        vegetation = Biomes.DESERT;
      else if (bionoise < 0.50)
        vegetation = Biomes.GRASSLAND;
      else if (bionoise < 0.83)
        vegetation = Biomes.FOREST;
      else
        vegetation = Biomes.RAIN_FOREST;
      return;
    }

    terrain = Terrains.PLAIN;
    if (bionoise < 0.16)
      vegetation = Biomes.DESERT;
    else if (bionoise > 0.66)
      vegetation = Biomes.JUNGLE;
    else
      vegetation = Biomes.GRASSLAND;
    return;
  }

  @override
  toString({bool pos=false}) =>
      "(${terrain.name.substring(0, 2)},${vegetation.name.substring(0, 2)} ${pos ? "($q,$r)" : ""})";

  void waterlevelInc({required double inc}) {
    //elevation = addElevation(inc: inc);
    Terrains terrback = terrain;
    Biomes vegback = vegetation;
    waterlevel += inc;
    biome(noise: bionoise);
    if (terrback != terrain) print("Terrain passed from $terrback to $terrain");
    if (vegback != vegetation) print("Veg passed from $vegback to $vegetation");
  }
}
