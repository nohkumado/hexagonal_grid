import "dart:math";
import "hexmap.dart";
import "hex_layout.dart";
import "hex_orientation.dart";
import "offset_coordinate.dart";

///Implemented from:
/// https://www.redblobgames.com/grids/hexagons/implementation.html
/// using the axial coordinates. no need to store s
/// based on the work of Jonathan Beckman jonathantbeckman@gmail.com

enum HexDir { RR, RU, LU, LL, LD, RD }

class Hex {
  static List directions = [
    Hex._(1, 0), //0 = right
    Hex._(1, -1), //1 = right up
    Hex._(0, -1), //2 = left up
    Hex._(-1, 0),

    ///3 = left
    Hex._(-1, 1), //4 = left down
    Hex._(0, 1) //5 = right down
  ];

  final int q, r;

  get s => -q - r;

  ///Constructors and factories
  Hex._(this.q, this.r);
  Hex({required q, required r}) : this._(q, r);
  Hex.invalid() : this._(-1, -1);

  factory Hex.fromPoint(HexLayout layout, Point p,{bool debug = false}) {
    if(layout.size.x == 0 || layout.size.y == 0) throw Exception("can't create hex from invalid layout");
    HexOrientation M = layout.orientation;
    Point size = layout.size;
   double q = (M.b0 * p.x + M.b1 * p.y) / size.x;//use radius instead of diameter?
   double r = (M.b2 * p.x + M.b3 * p.y) / size.y;
    if(debug)print("fraw hex: $q,$r");
    Hex result = hex_round(q: q, r: r);

    if(debug)print("found hex: $result");
    return result;
  }

  factory Hex.fromOddR(OffsetCoordinate offset) {
    int q = (offset.col - (offset.row - (offset.row % 2)) / 2).toInt();
    int r = offset.row;

    return new Hex._(q, r);
  }
  factory Hex.fromEvenR(OffsetCoordinate offset) {
    int q = (offset.col - (offset.row + (offset.row % 2)) / 2).toInt();
    int r = offset.row;

    return new Hex._(q, r);
  }
  factory Hex.fromOddQ(OffsetCoordinate offset) {
    int r = (offset.row - (offset.col - (offset.col % 2)) / 2).toInt();
    int q = offset.col;

    return new Hex._(q, r);
  }
  factory Hex.fromEvenQ(OffsetCoordinate offset) {
    int r = (offset.row - (offset.col + (offset.col % 2)) / 2).toInt();
    int q = offset.col;

    return new Hex._(q, r);
  }

  /// methods
  ///
  int length(Hex other) {
    double dist = (other.q.abs() + other.r.abs() + other.s.abs()) / 2;
    int intdist = ((other.q.abs() + other.r.abs() + other.s.abs()) ~/ 2);
    //print("double  distance is $dist vs ${dist.toInt()}");
    if (dist - intdist * 1.0 != 0) {
      throw Exception("Distance is not an integer...");
    }
    return (intdist);
  }

  int distance(Hex other) {
    return length(this - other);
  }

  Hex neighbor(int direction, {Hexmap? map}) {
    direction = direction % 6;
    //assert(0 <= direction && direction < 6);
    Hex neighbor = this + directions[direction.abs()];
    if (map == null) return neighbor;
    return map.get(hex: neighbor) ?? neighbor;
  }

  Point<double> toPixel(HexLayout layout,{bool debug=false,bool center=false}) {
    HexOrientation M = layout.orientation;

    double x = (M.f0 * q + M.f1 * r) * layout.size.x;
    double y = (M.f2 * q + M.f3 * r) * layout.size.y;
    if(debug)print("to#PPixel : q,r:$q,$r= ($x,$y) with ${M.f0},${M.f1}  ${M.f2},${M.f3} and ${M}");

    return (center)?Point(x + layout.origin.x, y + layout.origin.y):
    Point(x, y);
  }

  //calculates the corener in function of the offset
  Point cornerOffset(HexLayout layout, int corner) {
    Point size = layout.size;
    double angle = 2.0 * pi * corner / 6 + layout.orientation.startAngle;
    return Point(size.x * cos(angle),
        size.y * sin(angle));
  }

  List<Point<double>> corners(HexLayout layout,{Point shifted = const Point(0,0), bool debug = false, bool doCenter = false}) {
    List<Point<double>> corners = [];

    Point<double> center = toPixel(layout, center:doCenter, debug: debug);
    for (int i = 0; i < 6; i++) {
      Point offset = cornerOffset(layout, i);
      corners.add(Point(center.x + offset.x+shifted.x, center.y + offset.y+shifted.y));
    }
    Point offset = cornerOffset(layout, 0);
    corners.add(Point(center.x + offset.x+shifted.x, center.y + offset.y+shifted.y));

    return corners;
  }

  @override
  toString() => "($q,$r)";

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is Hex &&
              runtimeType == other.runtimeType &&
              q == other.q &&
              r == other.r;

  @override
  int get hashCode => q.hashCode ^ r.hashCode;

  Hex operator +(Hex other) {
    return Hex._(q + other.q, r + other.r);
  }

  Hex operator -(Hex other) {
    return Hex._(q - other.q, r - other.r);
  }

  Hex operator *(int k) => Hex._(q * k, r * k);

  clone({required int q, required int r}) {
    return Hex._(q, r);
  }

  static Hex toHex(Point p, {required HexLayout layout, bool debug = false}) {
    if(debug)print("Hex::toHex incoming $p l: $layout");
    HexOrientation M = layout.orientation;
    Point pt = Point((p.x - layout.origin.x) / layout.size.x,
        (p.y - layout.origin.y) / layout.size.y);
    double q = M.b0 * pt.x + M.b1 * pt.y;
    double r = M.b2 * pt.x + M.b3 * pt.y;
    if(debug)print("***** pre roudning: $pt q:$q, r:$r");
    return hex_round(q: q, r: r, s: -q - r);
  }

  static Hex hex_round({required double q, required double r, double? s}) {
    s ??=  -q-r;
    int qp = q.round();
    int rp = r.round();
    int sp = s.round();
    double q_diff = (qp - q).abs();
    double r_diff = (r - r).abs();
    double s_diff = (s - s).abs();
    if (q_diff > r_diff && q_diff > s_diff) {
      qp = -rp - sp;
    } else if (r_diff > s_diff) {
      rp = -qp - sp;
    } else {
      sp = -qp - rp;
    }
    return Hex(q: qp, r: rp);
  }
}
