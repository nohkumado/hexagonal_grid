import 'dart:math';

import 'hex_orientation.dart';

class HexLayout {
  final HexOrientation orientation;
  final Point size;

  final Point origin;
  final bool isFlat;

  factory HexLayout(
          {required Point size, required Point origin, flat = true}) =>
      (flat)
          ? new HexLayout.flat(size, origin)
          : HexLayout.pointy(size, origin);

  HexLayout.pointy(this.size, this.origin)
      : isFlat = false,
        orientation = HexOrientation.pointy();
  //new HexOrientation(sqrt(3.0), sqrt(3.0) / 2.0, 0.0, 3.0 / 2.0, sqrt(3.0) / 3.0, -1.0 / 3.0, 0.0, 2.0 / 3.0, 0.5);

  HexLayout.flat(this.size, this.origin)
      : isFlat = true,
        orientation = HexOrientation.flat();
  //new HexOrientation(3.0 / 2.0, 0.0, sqrt(3.0) / 2.0, sqrt(3.0), 2.0 / 3.0, 0.0, -1.0 / 3.0, sqrt(3.0) / 3.0, 0.0);
  HexLayout copy({Point? size, Point? origin, bool? isFlat })
  {
       // Determine the effective `isFlat` value for the copy
    bool flat = isFlat ?? this.isFlat;
// Return a new instance using the appropriate constructor based on `flat`
    return flat
        ? HexLayout.flat(size ?? this.size, origin ?? this.origin)
        : HexLayout.pointy(size ?? this.size, origin ?? this.origin);
  }

  ///Use the following methods to get the respective hexagon size of a symmetric
  /// shape such as a circle or square, given the orientation.
  /// https://www.redblobgames.com/grids/hexagons/#size-and-spacing
  Point spacing(double size) => (isFlat)
      ? Point(2 * size, sqrt(3.0) * size)
      : Point(sqrt(3.0) * size, 2 * size);
  //static Point getOrientPointySizeFromSymmetricalSize(double size) {
  //  return Point(sqrt(3.0) * size, 2 * size);
  //}

  //static Point getOrientFlatSizeFromSymmetricalSize(double size) {
  //  return Point(2 * size, sqrt(3.0) * size);
  //}
  @override
  String toString() {
    return 'HexLayout{orientation: $orientation, size: $size, origin: $origin, isFlat: $isFlat}';
  }
}
