import 'dart:math';

import 'package:fast_noise/fast_noise.dart';

import 'hex.dart';
import 'hex_layout.dart';
import 'terrain_hex.dart';

class Hexmap {
  Map<String, Hex> map = {};
  int width = 10;
  int height = 10;
  bool valid = false;

  double waterlevel = .11;
  Rectangle<double>? enclosing;

  Iterable<Hex> get values => map.values;

  Hexmap._(
      {required this.width,
      required this.height,
      double? waterlevel,
      TerrainHex? sample}) {
    //print("called hexmap CTOR for $width x $height");
    if (waterlevel != null) this.waterlevel = waterlevel;
    buildStructure(width: width, height: height, sample: sample);
  }

  void buildStructure({int? width, int? height, Hex? sample}) {
    //print("Building structure with $sample of ${sample.runtimeType}");
    valid = true;

    width = width ?? this.width;
    height = height ?? this.height;
    //print("----HexMap: buildStructure $width x $height");

    int rad = width ~/ 2;
    int maxrad = height ~/ 2;

    map.clear();
    for (int r = -maxrad; r <= maxrad; r++) {
      int start = r <= 0 ? -rad - r : -rad;
      int end = r <= 0 ? rad : rad - r;

      for (int q = start; q <= end; q++) {
        final hex =
            (sample != null) ? sample.clone(q: q, r: r) : Hex(q: q, r: r);
        map["$q,$r"] = hex;
      }
    }
    this.width = width;
    this.height = height;
  }

  factory Hexmap.hexagon(
      {required int radius, int? maxheight, TerrainHex? sample}) {
    //print("++++ Factory HexMap: hexagon");
    Hexmap hexmap = Hexmap._(
        width: (radius > 0) ? 2 * radius : radius,
        height:
            maxheight != null && maxheight != 0 ? 2 * maxheight : 2 * radius,
        sample: sample);

    return hexmap;
  }

  Hex? get({int? q, int? r, Hex? hex}) {
    if (hex == null && (q == null || r == null))
      throw Exception("Either hex is not null or q and r are not null");
    if (hex != null) {
      return map["${hex.q},${hex.r}"];
    }
    return map['$q,$r'];
  }

  @override
  String toString() {
    int rad = width ~/ 2;
    int maxrad = height ~/ 2;
    StringBuffer result = StringBuffer();
    result.write(
        "--------------- HexMap of ${width}x${height} ----------------\n");
    for (int r = -maxrad; r <= maxrad; r++) {
      // Add spacing before each row
      for (int i = 0; i < r.abs(); i++) {
        result.write('.....');
      }

      int start = r <= 0 ? -rad - r : -rad;
      int end = r <= 0 ? rad : rad - r;

      for (int q = start; q <= end; q++) {
        final hex = get(q: q, r: r);
        if (hex != null) {
          result.write('$hex  '); // Add spacing between hexes
        } else {
          result.write('xxxxxx'); // Empty space if hex is not in the map
        }
      }
      result.write('\n');
    }
    return result.toString();
  }

  void addNoise({int? seed, Terrains? border}) {
    //print("addNoise $width x $height");
    //print("set boundaries to ${width+1}x${height+1}");
    //the higher the frequency more hills!
    //more octaves more terrain detail!
    //usually octaves add to 1+.5+.25=1.75
    //TODO need a fudge factor, 6-8 looks nice, to test...
    List<List<double>> altnoise2d = noise2(width + 1, height + 1,
        noiseType: NoiseType.perlin,
        octaves: 3,
        frequency: 0.05,
        seed: seed ?? DateTime.now().millisecondsSinceEpoch);
    var bionoise2d = noise2(width + 1, height + 1,
        noiseType: NoiseType.perlin,
        octaves: 3,
        frequency: 0.05,
        seed: seed ?? DateTime.now().millisecondsSinceEpoch);

    int rad = width ~/ 2;
    int maxrad = height ~/ 2;
    List<double> noisemax = [0, 0, 0, 0];
    for (int r = -maxrad; r <= maxrad; r++) {
      int start = r <= 0 ? -rad - r : -rad;
      int end = r <= 0 ? rad : rad - r;

      for (int q = start; q <= end; q++) {
        altnoise2d[q + rad][r + maxrad] *=
            altnoise2d[q + rad][r + maxrad].isNegative ? -1 : 1;
        bionoise2d[q + rad][r + maxrad] *=
            bionoise2d[q + rad][r + maxrad].isNegative ? -1 : 1;
        if (altnoise2d[q + rad][r + maxrad] < noisemax[0])
          noisemax[0] = altnoise2d[q + rad][r + maxrad];
        if (altnoise2d[q + rad][r + maxrad] > noisemax[1])
          noisemax[1] = altnoise2d[q + rad][r + maxrad];
        if (bionoise2d[q + rad][r + maxrad] < noisemax[2])
          noisemax[2] = bionoise2d[q + rad][r + maxrad];
        if (bionoise2d[q + rad][r + maxrad] > noisemax[3])
          noisemax[3] = bionoise2d[q + rad][r + maxrad];
      }
    }
    //now we want values from 0 to 1
    double altdemult = 1;
    double biodemult = 1;
    double altspread = noisemax[1] - noisemax[0];
    double biospread = noisemax[3] - noisemax[2];
    //print("spread : $altspread");
    if (altspread < 0.7 || altspread > 1) {
      //print("alt spread OfR($altspread)....");
      altdemult = 1 / altspread;
    }
    if (biospread < 0.7 || biospread > 1) {
      //print("alt spread OfR($biospread)....");
      biodemult = 1 / biospread;
    }
    List<double> valsmax = [0, 0, 0, 0];
    for (int r = -maxrad; r <= maxrad; r++) {
      int start = r <= 0 ? -rad - r : -rad;
      int end = r <= 0 ? rad : rad - r;

      for (int q = start; q <= end; q++) {
        final hex = map["$q,$r"];
        if (hex is TerrainHex) {
          if (border != null &&
              ((q == start || q == end) || (r == -maxrad || r == maxrad))) {
            hex.vegetation = Biomes.DESERT;
            hex.terrain = border;
          } else {
            double altnoise = altdemult * (altnoise2d[q + rad][r + maxrad]);
            double bionoise = biodemult * (bionoise2d[q + rad][r + maxrad]);
            if (altnoise < valsmax[0]) valsmax[0] = altnoise;
            if (altnoise > valsmax[1]) valsmax[1] = altnoise;
            if (bionoise < valsmax[2]) valsmax[2] = bionoise;
            if (bionoise > valsmax[3]) valsmax[3] = bionoise;
            hex.addNoise(
                noise: altdemult * (altnoise2d[q + rad][r + maxrad]),
                bionoise: biodemult * (bionoise2d[q + rad][r + maxrad]),
                waterlevel: waterlevel);
          }
        } else
          print("wrong hextype to add noise : ${hex.runtimeType}");
      }
    }
    //print("######## or ranges = ${noisemax} vs ${valsmax}");
    //print("######## raw range of alt noise = ${noisemax[0]} to ${noisemax[1]}");
    //print("######## adjusted range of alt noise = ${altdemult*(altoff+noisemax[0])} to ${altdemult*(altoff+noisemax[1])}");
    //print("######## raw range of bio noise = ${noisemax[2]} to ${noisemax[3]}");
    //print("######## adjusted range of bio noise = ${biodemult*(biooff+noisemax[2])} to ${biodemult*(biooff+noisemax[3])}");
    this.width = width;
    this.height = height;
  }

  void waterlevelInc(double inc) {
    waterlevel += inc;
    int rad = width ~/ 2;
    int maxrad = height ~/ 2;
    //print("######## adjusted range of water level = $inc $rad x $maxrad");
    for (int r = -maxrad; r <= maxrad; r++) {
      int start = r <= 0 ? -rad - r : -rad;
      int end = r <= 0 ? rad : rad - r;

      for (int q = start; q <= end; q++) {
        final hex = map["$q,$r"];
        if (hex is TerrainHex) {
          hex.waterlevelInc(inc: inc);
        }
      }
    }
  }

  Hex? point2hex(double x, double y, {required HexLayout layout})
  {
    if(map.isNotEmpty)
      {
        return Hex.toHex(Point(x,y), layout:layout);
      }
    return null;
  }

  Rectangle<double> findGridCenter({required HexLayout layout})
  {
    double minX = double.infinity;
    double maxX = double.negativeInfinity;
    double minY = double.infinity;
    double maxY = double.negativeInfinity;

    // Iterate over each hex cell to find the bounding box
    for (Hex hex in map.values) {
      List<Point<double>> corners = hex.corners(layout);
      for (Point<double> corner in corners) {
        minX = min(minX, corner.x);
        maxX = max(maxX, corner.x);
        minY = min(minY, corner.y);
        maxY = max(maxY, corner.y);
      }
    }

    //i am baffled, the logic doesn't want to enter my brain, but inverting x and y works.....
    //enclosing = Rectangle(minX, minY, maxX-minX, maxY-minY);
    enclosing = Rectangle(minY, minX, maxY-minY, maxX-minX);
    return enclosing!;
  }
}
