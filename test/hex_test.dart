import 'dart:math';

import 'package:hexagonal_grid/hex.dart';
import 'package:hexagonal_grid/hex_layout.dart';
import 'package:hexagonal_grid/offset_coordinate.dart';
import 'package:test/test.dart';

void main() {
  group('Hex CTORS', () {
    test('Create Hex', () {
      final hex = Hex(q: 1, r: 2);
      expect(hex.q, 1);
      expect(hex.r, 2);
      expect(hex.s, -3); // s is calculated automatically
    });

    test('Invalid Hex', () {
      final invalidHex = Hex.invalid();
      expect(invalidHex.q, -1);
      expect(invalidHex.r, -1);
    });

    test('From Point - Flat Layout', () {
      final layout = HexLayout.flat(Point(10, 10), Point(0.0, 0.0));
      final point = Point(5.0, 8.66); // Example point within a flat hex

      Hex hex = Hex.fromPoint(layout, point);
      expect(hex.q, 1); // May vary slightly due to rounding
      expect(hex.r, 0);
      hex = Hex.fromPoint(layout, Point(15.0,26));
      expect(hex.q, 1, reason: "flat fromPoint q mismatch" ); // May vary slightly due to rounding
      expect(hex.r, 1, reason: "flat fromPoint r mismatch");
    });

    test('From Point - Pointy Layout', () {
      final layout = HexLayout.pointy(Point(10, 10), Point(0.0, 0.0));
      final point = Point(8.66, 5.0); // Example point within a pointy hex

      Hex hex = Hex.fromPoint(layout, point);
      expect(hex.q, 1); // May vary slightly due to rounding
      expect(hex.r, 0);
      hex = Hex.fromPoint(layout, Point(58,46.0));
      expect(hex.q, 2, reason: "pointy fromPoint q mismatch"); // May vary slightly due to rounding
      expect(hex.r, 3, reason: "pointy fromPoint r mismatch");
    });
    test('From OddR', () {
      final offset = OffsetCoordinate(3, 2);
      final hex = Hex.fromOddR(offset);

      expect(hex.q, 2);
      expect(hex.r, 2);
    });

    test('From EvenR', () {
      final offset = OffsetCoordinate(3, 3);
      final hex = Hex.fromEvenR(offset);
      expect(hex.q, 1);
      expect(hex.r, 3);
    });

    test('From OddQ', () {
      final offset = OffsetCoordinate(3, 2);
      final hex = Hex.fromOddQ(offset);

      expect(hex.q, 3);
      expect(hex.r, 1);
    });

    test('From EvenQ', () {
      final offset = OffsetCoordinate(3, 3);
      final hex = Hex.fromEvenQ(offset);

      expect(hex.q, 3);
      expect(hex.r, 1);
    });
    test('Hex Cloning', () {
      final hex = Hex(q: 1, r: 2);
      final clone = hex.clone(q: 3, r: -1);

      expect(clone.q, 3);
      expect(clone.r, -1);
      expect(hex.q, 1);
      expect(hex.r, 2); // Original hex should remain unchanged
    });
  });

  group('Hex methods neighbor', () {
    test('Distance - Neighbor', () {
      final hex1 = Hex(q: 1, r: 0);
      final hex2 = Hex(q: 2, r: 0); // Neighbor to the right

      final distance = hex1.distance(hex2);

      expect(distance, 1);
    });

    test('Distance - Non-Neighbor', () {
      final hex1 = Hex(q: 1, r: 0);
      final hex2 = Hex(q: 3, r: 2); // Further away

      final distance = hex1.distance(hex2);

      expect(distance, 4);
    });

    test('Neighbor - Flat Layout', () {
      final hex = Hex(q: 1, r: 2);
      //final layout = HexLayout.flat(Point(10, 0), Point(0.0, 0.0));

      final rightNeighbor = hex.neighbor(HexDir.RR.index, map: null);

      expect(rightNeighbor.q, 2);
      expect(rightNeighbor.r, 2);
    });

    test('Neighbor - Pointy Layout', () {
      final hex = Hex(q: 1, r: 2);
      //final layout = HexLayout.pointy(Point(10, 0), Point(0.0, 0.0));

      Hex rightNeighbor = hex.neighbor(HexDir.RR.index, map: null);
      expect(rightNeighbor.q, 2);
      expect(rightNeighbor.r, 2);
      rightNeighbor = hex.neighbor(HexDir.RD.index, map: null);
      expect(rightNeighbor.q, 1);
      expect(rightNeighbor.r, 3);
    });
  });

  group('Hex corners', () {
    test('Corner Offset flat', () {
      final layout = HexLayout.flat(Point(10, 10), Point(0.0, 0.0));
      final hex = Hex(q: 1, r: 2);

      final expectedOffsets = [
        Point(10.0, 0.0),   // Corner 0
        Point(5.0, 8.66),   // Corner 1
        Point(-5.0, 8.66),  // Corner 2
        Point(-10.0, 0.0),  // Corner 3
        Point(-5.0, -8.66), // Corner 4
        Point(5.0, -8.66),  // Corner 5
      ];

      for (int i = 0; i < 6; i++) {
        final corner = hex.cornerOffset(layout, i);
        expect(corner.x, closeTo(expectedOffsets[i].x, 0.01), reason: 'Corner $i x mismatch');
        expect(corner.y, closeTo(expectedOffsets[i].y, 0.01), reason: 'Corner $i y mismatch');
      }
    });

      test('Corner Offset pointy', () {
        final layout = HexLayout.pointy(Point(10, 10), Point(0.0, 0.0));
        final hex = Hex(q: 1, r: 2);

        final expectedOffsets = [
          Point(8.66, -5.0),   // Corner 0
          Point(8.66, 5.0),   // Corner 1
          Point(0.0, 10.0),  // Corner 2
          Point(-8.66, 5.0),  // Corner 3
          Point(-8.66, -5.0), // Corner 4
          Point(0.0, -10.0),  // Corner 5
        ];

        for (int i = 0; i < 6; i++) {
          final corner = hex.cornerOffset(layout, i);
          expect(corner.x, closeTo(expectedOffsets[i].x, 0.01), reason: 'Corner $i x mismatch');
          expect(corner.y, closeTo(expectedOffsets[i].y, 0.01), reason: 'Corner $i y mismatch');
        }
      });

      test('Corners pointy', () {
        final layout = HexLayout.pointy(Point(10, 10), Point(0.0, 0.0));
        final hex = Hex(q: 1, r: 2);

        final centerX = sqrt(3) * (1 + 2 / 2) * 10; // Center X
        final centerY = 1.5 * 2 * 10;              // Center Y

        final expectedCorners = [
          Point(centerX + 8.66, centerY -5),   // Corner 0
          Point(centerX + 8.66, centerY + 5.0),  // Corner 1
          Point(centerX , centerY + 10.0),  // Corner 2
          Point(centerX - 8.66, centerY + 5.0),  // Corner 3
          Point(centerX - 8.66, centerY - 5.0),  // Corner 4
          Point(centerX , centerY - 10.0),  // Corner 5
        ];

        final corners = hex.corners(layout);

        for (int i = 0; i < 6; i++) {
          expect(corners[i].x, closeTo(expectedCorners[i].x, 0.01), reason: 'Corner $i x mismatch');
          expect(corners[i].y, closeTo(expectedCorners[i].y, 0.01), reason: 'Corner $i y mismatch');
        }
      });


    test('Corners flat', () {
      final layout = HexLayout.flat(Point(10, 10), Point(0.0, 0.0));
      final hex = Hex(q: 1, r: 2);

      final centerX = (3 / 2) * 1 * 10; // Center X
      final centerY = sqrt(3) * (2 + 1 / 2) * 10; // Center Y

      final expectedCorners = [
        Point(centerX + 10.0, centerY + 0.0),   // Corner 0
        Point(centerX + 5.0, centerY + 8.66),  // Corner 1
        Point(centerX - 5.0, centerY + 8.66),  // Corner 2
        Point(centerX - 10.0, centerY + 0.0),  // Corner 3
        Point(centerX - 5.0, centerY - 8.66),  // Corner 4
        Point(centerX + 5.0, centerY - 8.66),  // Corner 5
      ];

      final corners = hex.corners(layout);

      for (int i = 0; i < 6; i++) {
        expect(corners[i].x, closeTo(expectedCorners[i].x, 0.01), reason: 'Corner $i x mismatch');
        expect(corners[i].y, closeTo(expectedCorners[i].y, 0.01), reason: 'Corner $i y mismatch');
      }
    });
  });

  group('Hex operators', () {

    test('Hex Addition', () {
      final hex1 = Hex(q: 1, r: 2);
      final hex2 = Hex(q: 3, r: -1);

      final sum = hex1 + hex2;

      expect(sum.q, 4);
      expect(sum.r, 1);
    });

    test('Hex Subtraction', () {
      final hex1 = Hex(q: 4, r: 2);
      final hex2 = Hex(q:1, r:1);

      final difference = hex1 - hex2;

      expect(difference.q, 3);
      expect(difference.r, 1);
    });

    test('Hex Multiplication', () {
      final hex = Hex(q: 2, r: -1);

      final multipliedHex = hex * 3;

      expect(multipliedHex.q, 6);
      expect(multipliedHex.r, -3);
    });

  });
  group('Hex methods toPixel', () {
    test('toPixel - Flat Layout', () {
      final layout = HexLayout.flat(Point(10, 10), Point(0.0, 0.0));

      Hex  hex = Hex(q: 0, r: 0);
      Point<double>pixel = hex.toPixel(layout);
      expect(pixel.x, closeTo(0.0, 0.01)); // May vary slightly
      expect(pixel.y, closeTo(0, 0.01));

      hex = Hex(q: 1, r: 0);
      pixel = hex.toPixel(layout);
      expect(pixel.x, closeTo(15.0, 0.01), reason: "flat x pixel mismatch"); // May vary slightly
      expect(pixel.y, closeTo(8.66, 0.01), reason: "flat y pixel mismatch");

      hex = Hex(q: 1, r: 2);
      pixel = hex.toPixel(layout);
      expect(pixel.x, closeTo(15.0, 0.01), reason: "flat 1,2 x pixel mismatch"); // May vary slightly
      expect(pixel.y, closeTo(43.3, 0.01), reason: "flat 1,2 y pixel mismatch");
    });

    test('toPixel - Pointy Layout', () {
      //final hex = Hex(q: 1, r: 2);
      final layout = HexLayout.pointy(Point(10, 10), Point(0.0, 0.0));

      //final pixel = hex.toPixel(layout);

      //expect(pixel.x, closeTo(sqrt(3.0) * 10.0, 0.01), reason: "pointy x pixel mismatch");
      //expect(pixel.y, closeTo(10.0, 0.01), reason: "pointy y pixel mismatch");
      Hex  hex = Hex(q: 0, r: 0);
      Point<double>pixel = hex.toPixel(layout);
      expect(pixel.x, closeTo(0.0, 0.01)); // May vary slightly
      expect(pixel.y, closeTo(0, 0.01));

      hex = Hex(q: 1, r: 0);
      pixel = hex.toPixel(layout);
      expect(pixel.x, closeTo(17.32, 0.01), reason: "pointy x pixel mismatch"); // May vary slightly
      expect(pixel.y, closeTo(0, 0.01), reason: "pointy y pixel mismatch");

      hex = Hex(q: 1, r: 2);
      pixel = hex.toPixel(layout);
      expect(pixel.x, closeTo(34.64, 0.01), reason: "pointy 1,2 x pixel mismatch"); // May vary slightly
      expect(pixel.y, closeTo(30, 0.01), reason: "pointy 1,2 y pixel mismatch");
    });
  });
  group('Hex toHex', () {
    test('Hex Round', () {
      // Test cases for various input values
      final cases = [
        [1.1, 2.2, -3.3],
        [-1.1, -2.2, 3.3],
        [0.5, -0.5, 0.0],
        [-0.5, 0.5, 0.0],
      ];

      for (final line in cases) {
        final q = line[0];
        final r = line[1];
        final s = line[2];

        final roundedHex = Hex.hex_round(q: q, r: r, s: s);

        // Assert the rounded values
        expect(roundedHex.q + roundedHex.r + roundedHex.s, 0,
            reason: "Sum of rounded coordinates should be 0");

        // Check if the rounded hex is the closest to the original
        final originalDistance = (q - roundedHex.q).abs() +
            (r - roundedHex.r).abs() +
            (s - roundedHex.s).abs();
        expect(originalDistance, lessThanOrEqualTo(1.0),
            reason: "Rounded hex should be close to original");
      }
    });

    test('ToHex and toPixel Round Trip', () {
      HexLayout layout = HexLayout.flat(Point(10,10), Point(0.0, 0.0));
      Hex hex = Hex(q: 1, r: 2);

      Point pixel = hex.toPixel(layout, debug:false);
      Hex hexFromPixel = Hex.fromPoint(layout, pixel, debug: false);

      expect(hexFromPixel.q, hex.q, reason: "flat round-trip q mismatch");
      expect(hexFromPixel.r, hex.r, reason: "flat round-trip r mismatch");

      layout = HexLayout.pointy(Point(10,10), Point(0.0, 0.0));
      pixel = hex.toPixel(layout);
      hexFromPixel = Hex.fromPoint(layout, pixel);
      expect(hexFromPixel.q, hex.q, reason: "pointy round-trip q mismatch");
      expect(hexFromPixel.r, hex.r, reason: "pointy round-trip r mismatch");
      //check rounding errors
      hex = Hex(q: 100, r: 200);
      pixel = hex.toPixel(layout);
      hexFromPixel = Hex.fromPoint(layout, pixel);
      expect(hexFromPixel.q, hex.q, reason: "pointy round-trip q mismatch");
      expect(hexFromPixel.r, hex.r, reason: "pointy round-trip r mismatch");
    });
  });
  //test('ToHex - Flat Layout (round trip)', ()
}//main