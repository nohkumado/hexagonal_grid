import 'dart:math';

import 'package:hexagonal_grid/hex_layout.dart';
import 'package:hexagonal_grid/hex_orientation.dart';
import 'package:test/test.dart';

void main() {
  group('HexLayout Tests', () {
    test('Create Pointy Layout', () {
      final point = Point(10.0, 10.0);
      final origin = Point(5.0, 5.0);

      final layout = HexLayout.pointy(point, origin);

      expect(layout.orientation, HexOrientation.pointy());
      expect(layout.size, point);
      expect(layout.origin, origin);
      expect(layout.isFlat, false);
    });

    test('Create Flat Layout', () {
      final point = Point(10.0, 10.0);
      final origin = Point(5.0, 5.0);

      final layout = HexLayout.flat(point, origin);

      expect(layout.orientation, HexOrientation.flat());
      expect(layout.size, point);
      expect(layout.origin, origin);
      expect(layout.isFlat, true);
    });

    test('Copy with default arguments', () {
      final layout = HexLayout.pointy(Point(10, 10), Point(5, 5));
      final copy = layout.copy();

      expect(copy.orientation, layout.orientation);
      expect(copy.size, layout.size);
      expect(copy.origin, layout.origin);
      expect(copy.isFlat, layout.isFlat);
    });

    test('Copy with custom arguments', () {
      final layout = HexLayout.pointy(Point(10, 10), Point(5, 5));
      final newSize = Point(20, 20);
      final newOrigin = Point(10, 10);

      final copy = layout.copy(size: newSize, origin: newOrigin);

      expect(copy.orientation, layout.orientation);
      expect(copy.size, newSize);
      expect(copy.origin, newOrigin);
      expect(copy.isFlat, layout.isFlat);
    });

    test('Spacing - Pointy', () {
      final layout = HexLayout.pointy(Point(10,0), Point(0.0, 0.0));

      final spacing = layout.spacing(5.0);

      expect(spacing.y, closeTo(10.0, 0.01)); // 2 * size
      expect(spacing.x, closeTo(sqrt(3.0) * 5.0, 0.01));
    });

    test('Spacing - Flat', () {
      final layout = HexLayout.flat(Point(10,0), Point(0.0, 0.0));

      final spacing = layout.spacing(5.0);

      expect(spacing.y, closeTo(sqrt(3.0) * 5.0, 0.01));
      expect(spacing.x, closeTo(10.0, 0.01)); // 2 * size
    });
  });
}