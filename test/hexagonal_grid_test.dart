import 'dart:math';

import 'package:hexagonal_grid/terrain_hex.dart';
import 'package:test/test.dart';
import 'package:hexagonal_grid/hexmap.dart';
import 'package:hexagonal_grid/hex.dart';
import 'package:hexagonal_grid/hex_layout.dart';


void main() {
  test('Origin hex to pixel works', () {
    final Point size = Point(2, 2);
    final Point hexLayoutOrigin = Point(0, 0);
    final Hex originHex = Hex(q: 0, r: 0);


    final HexLayout hexLayout = HexLayout.flat(size, hexLayoutOrigin);
    final Point hexToPixel = originHex.toPixel(hexLayout);

    expect(hexToPixel.x, hexLayoutOrigin.x);
    expect(hexToPixel.y, hexLayoutOrigin.y);
  });
  test('map tests', () {
    ///full hexagon
    Hexmap hexagonal = Hexmap.hexagon(radius: 3);
    String result = '''--------------- HexMap of 6x6 ----------------
...............(0,-3)  (1,-3)  (2,-3)  (3,-3)  
..........(-1,-2)  (0,-2)  (1,-2)  (2,-2)  (3,-2)  
.....(-2,-1)  (-1,-1)  (0,-1)  (1,-1)  (2,-1)  (3,-1)  
(-3,0)  (-2,0)  (-1,0)  (0,0)  (1,0)  (2,0)  (3,0)  
.....(-3,1)  (-2,1)  (-1,1)  (0,1)  (1,1)  (2,1)  
..........(-3,2)  (-2,2)  (-1,2)  (0,2)  (1,2)  
...............(-3,3)  (-2,3)  (-1,3)  (0,3)  
''';
    //print("first test: $hexagonal");
    expect(hexagonal.toString(), result);

    ///vertically squashed  hexagon as in e.g. xconq
    hexagonal = Hexmap.hexagon(radius: 3, maxheight: 2);
    result = '''--------------- HexMap of 6x4 ----------------
..........(-1,-2)  (0,-2)  (1,-2)  (2,-2)  (3,-2)  
.....(-2,-1)  (-1,-1)  (0,-1)  (1,-1)  (2,-1)  (3,-1)  
(-3,0)  (-2,0)  (-1,0)  (0,0)  (1,0)  (2,0)  (3,0)  
.....(-3,1)  (-2,1)  (-1,1)  (0,1)  (1,1)  (2,1)  
..........(-3,2)  (-2,2)  (-1,2)  (0,2)  (1,2)  
''';
    //print("second test $hexagonal");
    expect(hexagonal.toString(), result);
    Hex origin = hexagonal.get(q: 0, r: 0) ?? Hex(q: -1, r: -1);
    expect(origin.q, 0);
    expect(origin == origin, true);
    Hex upperleft = hexagonal.get(q: -1, r: -1) ?? Hex(q: 0, r: 0);
    expect(upperleft.q, -1);
    expect(origin != upperleft, true);
    Hex lowerright = hexagonal.get(q: 1, r: 1) ?? Hex(q: 0, r: 0);
    expect(lowerright.q, 1);
    int dist = upperleft.distance(lowerright);
    expect(dist, 4);
    Hex neighbor = upperleft.neighbor(HexDir.LD.index);
    expect(neighbor.q, -2);
    neighbor = upperleft.neighbor(HexDir.RD.index, map: hexagonal);
    expect(neighbor.q, -1);
    dist = origin.distance(neighbor);
    expect(dist, 1);
    print("End of tests for hexmap ${hexagonal.width} * ${hexagonal.height}");
    hexagonal =
        Hexmap.hexagon(radius: 5, maxheight: 3, sample: TerrainHex(q: 0, r: 0));

    hexagonal.addNoise(seed: 6733);
    result = '''--------------- HexMap of 10x6 ----------------
...............(SN,GR)  (SN,GR)  (SN,GR)  (HI,TA)  (HI,TA)  (PL,RA)  (PL,RA)  (PL,RA)  
..........(SN,GR)  (SN,GR)  (HI,TA)  (HI,TA)  (HI,TA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  
.....(HI,TA)  (HI,TA)  (HI,TA)  (HI,TA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  
(PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  
.....(PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,JU)  (PL,JU)  (PL,JU)  (PL,JU)  
..........(PL,JU)  (PL,JU)  (PL,JU)  (PL,JU)  (PL,JU)  (PL,JU)  (PL,JU)  (PL,JU)  (PL,JU)  
...............(BE,GR)  (BE,GR)  (BE,GR)  (BE,GR)  (BE,GR)  (BE,GR)  (BE,GR)  (BE,GR)  
''';
    expect(hexagonal.toString(), result);

    hexagonal.addNoise(seed: 6733, border: Terrains.LIMIT);
    result = '''--------------- HexMap of 10x6 ----------------
...............(LI,DE)  (LI,DE)  (LI,DE)  (LI,DE)  (LI,DE)  (LI,DE)  (LI,DE)  (LI,DE)  
..........(LI,DE)  (SN,GR)  (HI,TA)  (HI,TA)  (HI,TA)  (PL,RA)  (PL,RA)  (PL,RA)  (LI,DE)  
.....(LI,DE)  (HI,TA)  (HI,TA)  (HI,TA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (LI,DE)  
(LI,DE)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (LI,DE)  
.....(LI,DE)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,RA)  (PL,JU)  (PL,JU)  (PL,JU)  (LI,DE)  
..........(LI,DE)  (PL,JU)  (PL,JU)  (PL,JU)  (PL,JU)  (PL,JU)  (PL,JU)  (PL,JU)  (LI,DE)  
...............(LI,DE)  (LI,DE)  (LI,DE)  (LI,DE)  (LI,DE)  (LI,DE)  (LI,DE)  (LI,DE)  
''';
    expect(hexagonal.toString(), result);
    print("End of map tests ");
    //print("terrain hexmap ${hexagonal.width} * ${hexagonal.height}\n$hexagonal");
  });
}
