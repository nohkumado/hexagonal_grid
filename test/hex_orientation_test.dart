import 'dart:math';

import 'package:hexagonal_grid/hex_orientation.dart';
import 'package:test/test.dart';

void main() {
  group('HexOrientation Tests', () {
    test('Pointy Orientation', () {
      final pointyOrientation = HexOrientation.pointy();

      // Check the values of the orientation constants
      expect(pointyOrientation.f0, closeTo(HexOrientation.sqrt3, 0.001));
      expect(pointyOrientation.f1, closeTo(HexOrientation.sqrt3 / 2.0, 0.001));
      // ... other assertions for other constants

      // Check the start angle
      expect(pointyOrientation.startAngle, -pi/6);
    });

    test('Flat Orientation', () {
      final flatOrientation = HexOrientation.flat();

      // Check the values of the orientation constants
      expect(flatOrientation.f0, closeTo(3.0 / 2.0, 0.001));
      expect(flatOrientation.f1, closeTo(0.0, 0.001));
      // ... other assertions for other constants

      // Check the start angle
      expect(flatOrientation.startAngle, 0.0);
    });
  });
}